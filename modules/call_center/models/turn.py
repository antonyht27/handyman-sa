# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Turn(models.Model):
    _name = 'turn'
    _description = 'Generate Turn'

    name = fields.Char()
    description = fields.Text('Descripción')

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
         'El nombre del Giro ya existe !'),
    ]