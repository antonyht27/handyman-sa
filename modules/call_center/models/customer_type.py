# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CustomerType(models.Model):
    _name = 'customer.type'
    _description = 'Generate Customer type'

    name = fields.Char()
    description = fields.Text('Descripción')

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
         'Nombre de tipo de cliente ya Existe!'),
    ]