# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CrmLeadInherit(models.Model):
    _inherit = 'crm.lead'

    customer_request = fields.Char(string="Solicitud de Cliente")
    team_id = fields.Many2one('crm.team', string='Equipo de servicios', default=False, index=True, tracking=True,
                              required=True)

    # se quita el usuario por defecto para que no afecte el team_id por default
    user_id = fields.Many2one('res.users', string='Salesperson', index=True, tracking=True, default=False)

    # relacion con productos para agregar productos a la
    product_service = fields.Many2many('product.template', string='Servicio', domain="[('type', '=', 'service')]")

    @api.onchange('team_id')
    def _set_liable(self):
        if self.team_id:
            if self.team_id.user_id:
                self.user_id = self.team_id.user_id
                return {
                    'domain': {'user_id': [('id', '=', self.team_id.user_id.id)]},
                }
            else:
                self.user_id = None

    @api.onchange('user_id')
    def _onchange_user_id(self):
        """ When changing the user, also set a team_id or restrict team id to the ones user_id is member of.
        CANCELAMOS ESTA FUNCION GENERA PROBLEMAS CON EL ONCHANGE DE TEAM_ID
        """
        pass

    @api.model
    def create(self, vals):
        """
        Sobrecarcamos la funcion para asignar el responsable desde la vista KAnBAn
        :param vals:
        :return:
        """
        leader_id = self._get_leader_group(vals['team_id'])
        if leader_id:
            vals.update(leader_id)
        return super(CrmLeadInherit, self).create(vals)

    def _get_leader_group(self, team_id):
        obj_team = self.env['crm.team'].search([('id', '=', team_id)])
        if obj_team.user_id:
            return {'user_id': obj_team.user_id.id}
        else:
            return False

    def action_new_quotation(self):
        action = self.env.ref("sale_crm.sale_action_quotations_new").read()[0]
        product_service = self.mapped_product_service()
        action['context'] = {
            'search_default_opportunity_id': self.id,
            'default_opportunity_id': self.id,
            'search_default_partner_id': self.partner_id.id,
            'default_partner_id': self.partner_id.id,
            'default_team_id': self.team_id.id,
            'default_campaign_id': self.campaign_id.id,
            'default_medium_id': self.medium_id.id,
            'default_origin': self.name,
            'default_source_id': self.source_id.id,
            'default_company_id': self.company_id.id or self.env.company.id,
            'default_tag_ids': self.tag_ids.ids,
            'default_order_line': product_service
        }
        return action

    def mapped_product_service(self):
        values = []
        if self.product_service:
            for record in self.product_service:
                values.append((0, False, {
                    'name': record.name,
                    'product_id': record.id,
                    'product_uom_qty': 1.0,
                    'product_uom': record.uom_id.id,
                    'price_unit': record.list_price,
                }))
        return values
