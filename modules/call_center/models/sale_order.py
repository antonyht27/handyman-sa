# -*- coding: utf-8 -*-

from odoo import models, fields, api


class SaleOrderInherit(models.Model):
    _inherit = 'sale.order'

    def action_confirm(self):
        res = super(SaleOrderInherit, self).action_confirm()
        if self.opportunity_id:
            self.opportunity_id.action_set_won()
        return res

    def action_cancel(self):
        res = super(SaleOrderInherit, self).action_cancel()
        if self.opportunity_id:
            self.opportunity_id.action_set_lost()
        return res
