# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ResPartnerInherit(models.Model):
    _inherit = 'res.partner'

    phone = fields.Char(required=True)
    email = fields.Char(required=True)
    street = fields.Char(required=True)
    street2 = fields.Char(required=True)
    zip = fields.Char(change_default=True, required=True)
    city = fields.Char(required=True)
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict',
                               domain="[('country_id', '=?', country_id)]", required=True)
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict', required=True)

    # Many 2 One
    turn_id = fields.Many2one("turn", string='Giro')
    customer_type_id = fields.Many2one("customer.type", string='Tipo de cliente')
