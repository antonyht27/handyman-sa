# -*- coding: utf-8 -*-
{
    'name': "call center",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Antony H.",
    'website': "http://www.yourcompany.com",
    'category': 'Sales/CRM',
    'version': '13.0.1.0.0',

    # any module necessary for this one to work correctly
    'depends': ['sale_crm'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/service_team.xml',
        'views/res_partner.xml',
        'views/crm_lead.xml',
        'report/sale_order_report.xml',
    ],
    # only loaded in demonstration mode
    'demo': [   ],
    'license': 'AGPL-3',
    'installable': True,
    'auto_install': False,
    'application': False,
}
